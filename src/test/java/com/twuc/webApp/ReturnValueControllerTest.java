package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ReturnValueControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_request_method_return_void() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_204_when_request_custom_response_code() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_successfully_when_valid_response() throws Exception {
        mockMvc.perform(get("/api/messages/hi"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("hi"));
    }

    @Test
    void should_return_200_when_response_is_object() throws Exception {
        mockMvc.perform(get("/api/message-objects/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("hello"));
    }

    @Test
    void should_return_200_when_response_is_array() throws Exception {
        mockMvc.perform(get("/api/message-arrays/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].value").value("hello"));
    }

    @Test
    void should_return_202_when_response_is_object() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/hello"))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("hello"));
    }

    @Test
    void should_return_200_when_return_is_response_entity() throws Exception {
        mockMvc.perform(get("/api/message-entities/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.value").value("hello"))
                .andExpect(header().string("X-Auth", "me"));
    }

}
