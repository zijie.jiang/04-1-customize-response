package com.twuc.webApp;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ReturnValueControllerServerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_500_given_runtime_exception() {
        String testUrl = "/api/exceptions/runtime-exception";
        ResponseEntity<Void> entity = testRestTemplate.getForEntity(testUrl, Void.class);
        assertEquals(500, entity.getStatusCodeValue());
    }
}
