package com.twuc.webApp.model;

public class Message {
    private String value;

    public Message() {

    }

    public Message(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
