package com.twuc.webApp;

import com.twuc.webApp.model.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
@RequestMapping("/api")
public class ReturnValueController {

    @GetMapping("/no-return-value")
    public void getResponseCode() {
    }

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(NO_CONTENT)
    public void returnValueWithAnnotation() {
    }

    @GetMapping("/messages/{message}")
    public String returnMessage(@PathVariable String message) {
        return message;
    }

    @GetMapping("/message-objects/{message}")
    public Message returnMessageObject(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-arrays/{message}")
    public List returnMessageArray(@PathVariable String message) {
        return Collections.singletonList(new Message(message));
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(ACCEPTED)
    public Message returnMessageObjectWithAnnotation(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-entities/{message}")
    public ResponseEntity<Message> getResponseEntity(@PathVariable String message) {
        return ResponseEntity.ok()
                .header("X-Auth", "me")
                .contentType(APPLICATION_JSON)
                .body(new Message(message));
    }

    @GetMapping("/exceptions/runtime-exception")
    public void throwRuntimeExp() {
        throw new RuntimeException();
    }
}

